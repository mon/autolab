$skipZones = "Dateline", "UTC", "UTC-02", "UTC-11", "UTC+12", "GMT", "Greenwich"

New-ADOrganizationalUnit -Name "Units"

Get-ChildItem "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones" | ForEach-Object {

	if((Get-Random -Maximum 2) -eq 0) { return }

	$item = Get-ItemProperty $_.PSPath
        
	$zone = $item.PSChildName.Replace("Standard Time", "").Trim()
	$zone = $zone.Replace("  ", " ")

	if ($skipZones -contains $zone) { return }

	New-ADOrganizationalUnit -Name $zone -Path "OU=Units,DC=lab,DC=local"
        
	$display = $item.Display -Replace "\([^\)]+\)"

	$display.Split(",") | ForEach-Object {
		if((Get-Random -Maximum 5) -eq 0) { return }

		$location = $_.Trim()
		New-ADOrganizationalUnit -Name $location -Path "OU=$zone,OU=Units,DC=lab,DC=local"
		New-ADOrganizationalUnit -Name "Users"   -Path "OU=$location,OU=$zone,OU=Units,DC=lab,DC=local"
		New-ADOrganizationalUnit -Name "Groups" -Path "OU=$location,OU=$zone,OU=Units,DC=lab,DC=local"
		New-ADOrganizationalUnit -Name "Computers" -Path "OU=$location,OU=$zone,OU=Units,DC=lab,DC=local"
		New-ADOrganizationalUnit -Name "Servers" -Path "OU=$location,OU=$zone,OU=Units,DC=lab,DC=local"
	}
}
