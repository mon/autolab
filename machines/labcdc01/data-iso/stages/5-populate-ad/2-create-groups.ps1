Get-ChildItem "HKLM:\\SOFTWARE\Microsoft\Windows NT\CurrentVersion" | ForEach-Object {

    $name = Split-Path $_.PSPath -Leaf

    $groupscope = Get-Random -Maximum 3

    New-ADGroup -Name $name -GroupScope $groupscope -Path "OU=Units,DC=lab,DC=local"
}
