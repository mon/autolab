$ignoreUsers = "Administrator", "Guest", "krbtgt"
$ignoreUsersFilter = "SamAccountName -ne '$($ignoreUsers -join "' -and SamAccountName -ne '")'"

[array]$users = Get-ADUser -Filter $ignoreUsersFilter -SearchBase "CN=Users,DC=lab,DC=local"
[array]$computers = Get-ADComputer -Filter "*" -Properties "*" -SearchBase "CN=Computers,DC=lab,DC=local"

if ($users.Length) {

	$userOrganizationalUnits = Get-ADOrganizationalUnit -Filter 'Name -like "Users"' -SearchBase "OU=Units,DC=lab,DC=local"
}

if ($computers.Length) {

	$computerOrganizationalUnits = Get-ADOrganizationalUnit -Filter 'Name -like "Computers"' -SearchBase "OU=Units,DC=lab,DC=local"
	$serverOrganizationalUnits = Get-ADOrganizationalUnit -Filter 'Name -like "Servers"' -SearchBase "OU=Units,DC=lab,DC=local"
}

foreach ($user in $users) {

	$targetOrganizationalUnit = $userOrganizationalUnits[(Get-Random -Maximum $userOrganizationalUnits.Count)]

	Move-ADObject -Identity $user.DistinguishedName -TargetPath $targetOrganizationalUnit.DistinguishedName
}

foreach ($computer in $computers) {

	if ($computer.OperatingSystem.Contains("Windows Server")) {

		$targetOrganizationalUnit = $serverOrganizationalUnits[(Get-Random -Maximum $serverOrganizationalUnits.Count)]
	} else {

		$targetOrganizationalUnit = $computerOrganizationalUnits[(Get-Random -Maximum $computerOrganizationalUnits.Count)]
	}

	Move-ADObject -Identity $computer.DistinguishedName -TargetPath $targetOrganizationalUnit.DistinguishedName
}
