New-Item -Type "directory" -Force "$env:ProgramData\autolab"
Copy-Item "$PSScriptRoot\service\Move-ADObjects.ps1" "$env:ProgramData\autolab\"

schtasks.exe /Create /F /RU "SYSTEM" /SC minute /MO 1 /TN "move-adobjects" /TR "powershell.exe -File $env:ProgramData\autolab\Move-ADObjects.ps1"
