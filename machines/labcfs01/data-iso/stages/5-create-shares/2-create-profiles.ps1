New-Item C:\storage\profiles -Type directory

New-SmbShare -Path C:\storage\profiles -Name profiles -Description "Profile share" -FullAccess Everyone

$acl = New-Object System.Security.AccessControl.DirectorySecurity
$ar = New-Object System.Security.AccessControl.FileSystemAccessRule("Everyone", "AppendData", "None", "None", "Allow")
$acl.SetAccessRule($ar)
Set-Acl -Path "C:\storage\profiles" -AclObject $acl
