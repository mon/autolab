New-Item C:\storage\tmp -Type directory

New-SmbShare -Path C:\storage\tmp -Name tmp$ -Description "Temp share" -FullAccess Everyone

$acl = New-Object System.Security.AccessControl.DirectorySecurity
$ar = New-Object System.Security.AccessControl.FileSystemAccessRule("LAB\Domain Users", "FullControl", "ContainerInherit, ObjectInherit", "None", "Allow")
$acl.SetAccessRule($ar)
Set-Acl -Path "C:\storage\tmp" -AclObject $acl

New-Item C:\storage\tmp\admin\test\ -Type directory
Copy-Item -Path "$(Split-Path -Path $PSScriptRoot -Qualifier)\Autounattend.xml" -Destination C:\storage\tmp\admin\test\
New-Item C:\storage\tmp\admin\test\install\ -Type directory
Copy-Item -Path C:\Windows\Panther\* -Destination C:\storage\tmp\admin\test\install\
