New-Item C:\storage\common -Type directory

New-SmbShare -Path C:\storage\common -Name common -Description "Common share" -FullAccess Everyone

$acl = New-Object System.Security.AccessControl.DirectorySecurity
$ar = New-Object System.Security.AccessControl.FileSystemAccessRule("NT AUTHORITY\Authenticated Users", "ReadAndExecute", "ContainerInherit", "None", "Allow")
$acl.SetAccessRule($ar)
Set-Acl -Path "C:\storage\common" -AclObject $acl

New-Item C:\storage\common\Install\ -Type directory
Copy-Item -Path D:\setup.exe -Destination C:\storage\common\Install\
New-Item C:\storage\common\Temporary\ -Type directory
Copy-Item -Path C:\Windows\Temp\* -Destination C:\storage\common\Temporary\
New-Item C:\storage\common\Temporary\old\IT\ -Type directory
Copy-Item -Path C:\Windows\Panther\* -Destination C:\storage\common\Temporary\old\IT\

$acl = New-Object System.Security.AccessControl.DirectorySecurity
$ar = New-Object System.Security.AccessControl.FileSystemAccessRule("LAB\Domain Users", "Modify", "ContainerInherit, ObjectInherit", "None", "Allow")
$acl.SetAccessRule($ar)
Set-Acl -Path "C:\storage\common\Temporary" -AclObject $acl
