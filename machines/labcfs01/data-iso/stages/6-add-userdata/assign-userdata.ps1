$username = "LAB\Administrator"
$password = "Pass@word01"
$skipusers = "Administrator", "Guest"

$securepassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential $username,$securepassword

Get-ADUser -Credential $credential -Filter * | %{
	$un = $_.SamAccountName

	if ($skipusers -notcontains $un ) {
		New-Item "C:\storage\home\$un" -Type directory
		$acl = New-Object System.Security.AccessControl.DirectorySecurity
		$ar = New-Object System.Security.AccessControl.FileSystemAccessRule("LAB\$un", "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
		$acl.SetAccessRule($ar)
		Set-Acl -Path "C:\storage\home\$un" -AclObject $acl

		Set-ADUser -Credential $credential -Identity $un -HomeDrive "H:" -HomeDirectory "\\$env:COMPUTERNAME\home\$un"
		Set-ADUser -Credential $credential -Identity $un -ProfilePath "\\$env:COMPUTERNAME\profiles\$un"
	}
}
