Copy-Item -Path "$PSScriptRoot\{*}" -Destination "C:\Windows\Temp" -Recurse

# Replace servername
$xmlfilepath = "C:\Windows\Temp\{DBE1B81C-9F09-49F1-9C31-C93034AC2EBA}\DomainSysvol\GPO\User\Preferences\Drives\Drives.xml"
(Get-Content $xmlfilepath).Replace("SERVERNAME", $env:COMPUTERNAME) | Set-Content $xmlfilepath -Force

$gpo = New-GPO -Name "00cu-MapDrives-Common-W"
Import-GPO -TargetGuid $gpo.Id -BackupId "DBE1B81C-9F09-49F1-9C31-C93034AC2EBA" -Path "C:\Windows\Temp"
$gpo.Computer.Enabled = $false
New-GPLink -Guid $gpo.Id -Target "OU=Units,DC=lab,DC=local"

schtasks /Delete /TN "GPOImport" /F
