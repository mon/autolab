NAME = autolab
VERSION = 0.5

default:

install:
	install -d $(DESTDIR)/var/lib/$(NAME)
	install -p -m 755 $(NAME) $(DESTDIR)/usr/bin/$(NAME)
	cp -r machines $(DESTDIR)/var/lib/$(NAME)/
	cp -r networks $(DESTDIR)/var/lib/$(NAME)/
	cp -r templates $(DESTDIR)/var/lib/$(NAME)/

archive:
	@git archive --prefix=$(NAME)-$(VERSION)/ HEAD --format=tar.gz \
	 -o $(NAME)-$(VERSION).tar.gz
	@echo "$(NAME)-$(VERSION).tar.gz created"

clean:
	rm -f *~ *tar.gz
